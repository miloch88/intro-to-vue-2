var eventBus = new Vue();

Vue.component('product',{
    props:{
        premium:{
            type:Boolean,
            required: true
        }
    },
    
    template: `      
    <div class="product">
        <div class="product-image">
            <img v-bind:src="image" v-bind:alt="altText" />
        </div>
        <div class="product-info">
            <h1>{{title}} <span style="color:red">{{sale}}</span></h1>
        <a v-bind:href="linkAllegro" target="_blank"> Więcej podobnych </a>

        <p v-if="inStock">Na stanie</p>
        <!-- <p v-else-if="inventory <=10 && inventory > 0">Na wyczerpaniu</p> -->
        <p v-else :class="{outOfStock: !inStock}" Quiet Light for VSC>
            Brak towaru
        </p>

        <product-details :shipping="shipping" :details="details" :sizes="sizes"></product-details>

        <div
            class="color-box"
            v-for="(variant, index) in variants"
            :key="variant.variantId"
            :style="{ backgroundColor: variant.variantColor }"
            @mouseover="updateProduct(index)"
        ></div>

        <button
            v-on:click="addToCart"
            :disabled="!inStock"
            :class="{disabledButton: !inStock}"
        >
            Dodaj to koszyka
        </button>
        <button v-on:click="subFromCart">Usuń z koszyka</button>


        </div>

        <product-tabs :reviews="reviews"></product-tabs>

             
    </div>
  `,
  data(){
      return       {
        product: "Skarpetki",
        brand: "Vue Mastery",
        altText: "Para skarpetek",
        selectedVariant: 0,
        inventory: 11,
        linkAllegro: "https://allegro.pl/listing?string=skarpetki&bmatch=e2101-d3794-c3683-fas-1-1-0611",
        details: ["80% bawełna", "20% poliester", "Męskie/ Damskie"],
        variants: [
            {
                variantId: 2234,
                variantColor: "green",
                variantImage: "./assets/vmSocks-green-onWhite.jpg",
                variantQuantity: 5,
                onSale: false
              
    
            },
            {
                variantId: 2235,
                variantColor: "blue",
                variantImage: "./assets/vmSocks-blue-onWhite.jpg",
                variantQuantity: 3,
                onSale: true,
     
            }
        ],
        sizes: ["S","M","L","XL","XXL"],
        reviews: [],
    }
  },
methods:{
    addToCart(){
        this.$emit('add-to-cart', this.variants[this.selectedVariant].variantId)
    },
    updateProduct(index){
        this.selectedVariant = index;
    },
    subFromCart(){
        this.$emit('sub-from-cart', this.variants[this.selectedVariant].variantId)
    },
},
computed:{
    title(){
        return this.brand + ' ' + this.product
    },
    image(){
        return this.variants[this.selectedVariant].variantImage;
    },
    inStock(){
        return this.variants[this.selectedVariant].variantQuantity;
    },
    sale(){
        return this.variants[this.selectedVariant].onSale ? "Wyprzedaż!" : ""
    },
    shipping(){
        return this.premium ? "Darmowa" : "14,99"
    }
},
mounted(){
    eventBus.$on('review-submitted', productReview => {
            this.reviews.push(productReview)
    })
}

})

Vue.component('product-review',{
    template:
    `<form class="review-form" @submit.prevent="onSubmit">

    <p v-if="errors.length">
        <b> Wystąpiły następujące błędy: </b>
        <ul>
            <li v-for="error in errors">{{error}}</li>
        </ul>
    <p>

      <label for="name">Imię:</label>
      <input id="name" v-model="name" placeholder="name">
    </p>
    
    <p>
      <label for="review">Opinia:</label>      
      <textarea id="review" v-model="review"></textarea>
    </p>
    
    <p>
      <label for="rating">Ocena:</label>
      <select id="rating" v-model.number="rating">
        <option>5</option>
        <option>4</option>
        <option>3</option>
        <option>2</option>
        <option>1</option>
      </select>
    </p>

    <p>
        <label for="recomended">Czy polecił byś ten produkt? </label>
        <div>
            <input type="radio" id="recomendedYes" name="recomended" value="1" v-model="recomended">
            <label for="recomendedYes">Tak</label>
        </div><div>
            <input type="radio" id="recomendedNo" name="recomended" value="0" v-model="recomended">
            <label for="recomendedYes">Nie</label>
        </div>
    </p>
        
    <p>
      <input type="submit" value="Submit">  
    </p>    
  
  </form>`,
    data(){
        return{
            name: null,
            review: null,
            rating: null,
            recomended: null,
            errors: []
        }
    },
    methods:{
        onSubmit() {
            if(this.name && this.review && this.rating && this.recomended){
                let productReview = {
                    name: this.name,
                    review: this.review,
                    rating: this.rating,
                    recomended: this.recomended
                }
                eventBus.$emit('review-submitted', productReview);
                this.name = null
                this.review = null
                this.rating = null
                this.recomended = null
            }else{
                if(!this.name)this.errors.push("Imię jest obowiązkowe")
                if(!this.review)this.errors.push("Opinia jest obowiązkowa")
                if(!this.rating)this.errors.push("Ocena jest obowiązkowa")
                if(!this.recomended)this.errors.push("Rekomendacja jest obowiązkowa")
                
            }

        }
    }
})

Vue.component('product-tabs', {
    props:{
        reviews:{
            type: Array,
            required: true
        }
    },
    template:`
    <div>
        <span class="tab"
        :class="{activeTab: selectedTab === tab}"
        v-for="(tab, index) in tabs" 
        :key="index"
        @click="selectedTab = tab"
        >{{ tab }} 
        </span>

        <div v-show="selectedTab === 'Reviews'">
            <h2>Opinie</h2>
            <p v-if="!reviews.length">Brak opini. Możesz być pierwszy</p>
                <ul>
                    <li v-for="review in reviews">
                        <p>{{review.name}}</p>
                        <p>Ocena: {{review.rating}}</p>
                        <p>{{review.review}}</p>
                        <p>Rekomendacja: {{review.recomended == 1 ? "Tak" : "Nie"}}</p>
                    </li>
                </ul>
        </div>
        
        <product-review v-show="selectedTab === 'Make a Review'"></product-review>   
    </div>
    `,
    data(){
        return{
            tabs: ['Reviews', 'Make a Review'],
            selectedTab: 'Reviews'
        }
    }
})

Vue.component('product-details', {
    props:{
        shipping:{
            required: true
        },
        details:{
            type: Array,
            required: true
        },
        sizes:{
            type: Array,
            required:true
        }
    },
    template:`
    <div>
        <span class="tab"
        :class="{activeTab: selectedTab === tab}"
        v-for="(tab, index) in tabs"
        :key="index"
        @click="selectedTab = tab">
        {{tab}}
        </span>

        <div v-show="selectedTab === 'Shipping'">
            <p> Dostawa: {{shipping}}</p>
        </div>

        <div v-show="selectedTab === 'Details'">
            <ul>
                <li v-for="detail in details">{{ detail }}</li>
            </ul>

            <ul>
                <li v-for="size in sizes">{{ size }}</li>
            </ul>
        </div>

    </div>
    
    `,
    data(){
        return{
            tabs: ['Shipping', 'Details'],
            selectedTab: 'Shipping'
        }
    }
})

var app = new Vue({
    el: "#app",
    data : {
        premium: true,
        cart: []
    },
    methods:{
        updateCart(id){
            this.cart.push(id);
        },
        subCart(id){
            for(var i = this.cart.length - 1; i>=0; i--){
                if(this[i] === id){
                    this.cart.splice(i,1);
                }
            }
        }
    }
})